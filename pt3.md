### classes[7] = "Phylogenetics Pt. III - Distance methods + Parsimony"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](logo-ESTB.png)

Francisco Pina Martins

---

### Summary


* &shy;<!-- .element: class="fragment" -->Phylogenetic resconstruction methods
* &shy;<!-- .element: class="fragment" -->Theoretical exposition
* &shy;<!-- .element: class="fragment" -->Practical work

---

### Phylogenetic reconstruction methods

* &shy;<!-- .element: class="fragment" data-fragment-index="1" -->There are several methods for phylogenetic reconstruction
* &shy;<!-- .element: class="fragment" data-fragment-index="2" -->Those that use character states
    * &shy;<!-- .element: class="fragment" data-fragment-index="3" --><span class="fragment highlight-red" data-fragment-index="9">Maximum parsimony</span>
    * &shy;<!-- .element: class="fragment" data-fragment-index="4" -->Maximum likelihood
    * &shy;<!-- .element: class="fragment" data-fragment-index="5" -->Bayesian inference
* &shy;<!-- .element: class="fragment" data-fragment-index="6" -->Those that use distance measurements
    * &shy;<!-- .element: class="fragment" data-fragment-index="7" --><span class="fragment highlight-red" data-fragment-index="10">UPGMA</span>
    * &shy;<!-- .element: class="fragment" data-fragment-index="8" --><span class="fragment highlight-red" data-fragment-index="10">Neighbour joining</span>

---

### Parsimony methods

* &shy;<!-- .element: class="fragment" -->Assumes the minimum possible changes
* &shy;<!-- .element: class="fragment" -->It uses this premiss to perform phylogenetic reconstruction

|||

### Maximum parsimony

<img src="pt3_assets/MP_01.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="pt3_assets/MP_02.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="pt3_assets/MP_03.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="pt3_assets/MP_04.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="pt3_assets/MP_05.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="pt3_assets/MP_06.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="pt3_assets/MP_07.png" style="background:none; border:none; box-shadow:none;">

|||

### Maximum parsimony

<img src="pt3_assets/MP_08.png" style="background:none; border:none; box-shadow:none;">

---

### Consensus trees


* &shy;<!-- .element: class="fragment" -->What if there are ties for the best scoring tree?

<img src="pt3_assets/Consensus_01.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Strict consensus

<img src="pt3_assets/Consensus_02.png" style="background:none; border:none; box-shadow:none; width:79%">

|||

### Majority Rule consensus

<img src="pt3_assets/Consensus_03.png" style="background:none; border:none; box-shadow:none; width:79%">

|||

### Consensus trees

* &shy;<!-- .element: class="fragment" -->Strict
    * &shy;<!-- .element: class="fragment" -->Groups that occur on all trees 
* &shy;<!-- .element: class="fragment" -->Majority Rule
    * &shy;<!-- .element: class="fragment" -->Groups that occur on *X%* (50 - 99) trees
* &shy;<!-- .element: class="fragment" -->Semi-Strict
* &shy;<!-- .element: class="fragment" -->Adams
* &shy;<!-- .element: class="fragment" -->...

---

### Finding the best tree

<img src="pt3_assets/Possible_trees.png" style="background:none; border:none; box-shadow:none; width:79%">

|||

### Finding the best tree

* &shy;<!-- .element: class="fragment" -->In order to find the most parsimonious tree, all have to be tried
    * &shy;<!-- .element: class="fragment" -->...unless we "cheat"
* &shy;<!-- .element: class="fragment" -->In order to save computation time some shortcuts have been devised:
    * &shy;<!-- .element: class="fragment" -->Heuristic search
    * &shy;<!-- .element: class="fragment" -->Branch-and-bound search
    * &shy;<!-- .element: class="fragment" -->Quartets search
* &shy;<!-- .element: class="fragment" -->These methods sacrifice the guarantee of finding the best tree
    * &shy;<!-- .element: class="fragment" -->In exchange for shortening computation time

---

### Distance methods

* &shy;<!-- .element: class="fragment" -->Count the number of differences between *taxa*
    * &shy;<!-- .element: class="fragment" -->Fast
    * &shy;<!-- .element: class="fragment" -->Inaccurate
        * &shy;<!-- .element: class="fragment" -->Discard a lot of information
* &shy;<!-- .element: class="fragment" -->Can be corrected with sequence evolution models

|||

### Distance methods

<img src="pt3_assets/dist01.png" style="background:none; border:none; box-shadow:none;">

|||

### Distance methods

* &shy;<!-- .element: class="fragment" -->Once the distance matrix is calculated, several algorithms can be used to build a tree
    * &shy;<!-- .element: class="fragment" -->Neighbour-Joining (NJ)
    * &shy;<!-- .element: class="fragment" -->**U**nweighted **P**air **G**roup **M**ethod with **A**rithmetic mean (UPGMA)
    * &shy;<!-- .element: class="fragment" -->The choice of algorithm can heavily influence the obtained tree!

---

### Practical work

* Install the software [mpboot](http://www.iqtree.org/mpboot/)
* Build a Maximum Parsimony tree on each of the 3 gene fragments used in Akihito *et al.* 2016
* Install [FigTree](http://tree.bio.ed.ac.uk/software/Figtree/) and use it to visualize the treefile
    * Alternatively use [ToyTree](https://toytree.readthedocs.io/en/latest/4-tutorial.html) (Python experts only)
* Interpret the trees 
    * Find monophyletic groups
    * Compare the results with those from the original paper
    * Are the original hypotheses supported by your analyses?
* You can find a copy of the data [here](pt3_assets/Akihito_data.tar.xz) in case you have lost yours for some reason

---

### References

* [A very detailed paper on Maximum Parsimony](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3377548/)
* [A review paper on consensus trees](http://www.maths.otago.ac.nz/~dbryant/Papers/03ConsensusAMS.pdf)
* [Details on tree searching methods](https://doi.org/10.1002/0471250953.bi0604s00)
* [Beginner's guide to phylogenetics](https://link.springer.com/article/10.1007/s00248-013-0236-x)
* [Phylogenetic inference based on distance methods](https://www2.ib.unicamp.br/profs/sfreis/SistematicaMolecular/Aula08MetodosMatrizesDistancias/Leituras/ThePhylogeneticHandbookMatrizesDistancias.pdf)
* [MEGA Manual](https://www.megasoftware.net/web_help_10/index.htm#t=First_Time_User.htm)

### classes[5] = "Phylogenetics Pt. I"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](logo-ESTB.png)

Francisco Pina Martins

---

### Summary


* &shy;<!-- .element: class="fragment" -->What is phylogenetics
* &shy;<!-- .element: class="fragment" -->Revisiting DNA
* &shy;<!-- .element: class="fragment" -->Phylogenetics history
* &shy;<!-- .element: class="fragment" -->Phylogenetics concepts

&shy;<!-- .element: class="fragment" -->![Phylogenetic tree](pt1_assets/generic_phylo.jpg)

---

### What is phylogenetics?

* &shy;<!-- .element: class="fragment" -->A set of methods to infer relations between [OTUs](https://link.springer.com/article/10.1007/s12223-018-0627-y)
* &shy;<!-- .element: class="fragment" -->It's used in:
  * &shy;<!-- .element: class="fragment" -->Taxonomy
  * &shy;<!-- .element: class="fragment" -->Molecular dating
  * &shy;<!-- .element: class="fragment" -->Molecular evolution
  * &shy;<!-- .element: class="fragment" -->Gene transfer detection

&shy;<!-- .element: class="fragment" -->![Darwin's Phylo tree](pt1_assets/darwin.jpg)

---

### What do we need for phylogenetics?


* &shy;<!-- .element: class="fragment" -->Many different types of data:
 * &shy;<!-- .element: class="fragment" -->Morphological traits
 * &shy;<!-- .element: class="fragment" -->Behavioural traits
 * &shy;<!-- .element: class="fragment" -->Molecular sequences
   * &shy;<!-- .element: class="fragment" -->Amino Acid sequences
   * &shy;<!-- .element: class="fragment" --><span class="fragment highlight-red">DNA sequences</span>

&shy;<!-- .element: class="fragment" -->![Chromatogram](pt1_assets/chroma.png)

---

### Sequencing DNA

Sanger Method

<img src="pt1_assets/sanger.jpg" style="background:none; border:none; box-shadow:none;">

|||

### Sequencing DNA

Illumina method

<img src="pt1_assets/Illumina.png" style="background:none; border:none; box-shadow:none;">

|||

### Sequencing DNA

Oxford Nanopore

<img src="pt1_assets/nanopore.jpg" style="background:none; border:none; box-shadow:none;">

---

### Some genetics concepts

* &shy;<!-- .element: class="fragment" -->Differences between genomes are called **polymorphisms**
    * &shy;<!-- .element: class="fragment" -->Sequence polymorphisms
    * &shy;<!-- .element: class="fragment" -->Length polymorphisms
* &shy;<!-- .element: class="fragment" -->Alternate forms are called **alleles**
* &shy;<!-- .element: class="fragment" -->The combination of both alleles on an individual is called **genotype**
* &shy;<!-- .element: class="fragment" -->A **locus** (or loci, if plural) is a region of the genome
* &shy;<!-- .element: class="fragment" -->A **haplotype** is a group of alleles inherited from the same parent

&shy;<!-- .element: class="fragment" -->![Alleles, genotypes and haplotypes mockup](pt1_assets/al_gn_hap.png)

---

### The concept of evolution

&shy;<!-- .element: class="fragment" -->*Change in a population's heritable characteristics across generations*

* &shy;<!-- .element: class="fragment" -->Requires:
    * &shy;<!-- .element: class="fragment" -->Trait variability
    * &shy;<!-- .element: class="fragment" -->Heritability

&shy;<!-- .element: class="fragment" -->![Conus diversity](pt1_assets/diversity.webp)

---

### Mechanisms of evolution

<div style="float:left; width:75%">

* &shy;<!-- .element: class="fragment" -->Mutation
* &shy;<!-- .element: class="fragment" -->Gene-flow
* &shy;<!-- .element: class="fragment" -->Genetic drift
* &shy;<!-- .element: class="fragment" -->Non-random mating
* &shy;<!-- .element: class="fragment" -->[Natural] Selection

</div>
<div style="float:right; width:25%">

&shy;<!-- .element: class="fragment" -->![Charles Darwin](pt1_assets/darwin.webp)

</div>

---

### Mitochondrial DNA

<img src="pt1_assets/human_mito_map.png" width="40%" class="fragment" style="background:none; border:none; box-shadow:none; float:right">

* &shy;<!-- .element: class="fragment" -->Circular
* &shy;<!-- .element: class="fragment" -->~17Kbp
* &shy;<!-- .element: class="fragment" -->Independent replication
* &shy;<!-- .element: class="fragment" -->No recombination
* &shy;<!-- .element: class="fragment" -->Maternal inheritance
* &shy;<!-- .element: class="fragment" -->No repair
* &shy;<!-- .element: class="fragment" -->High mutation rate

---

### Phylogenetics history

---

### Cladistics

<img src="pt1_assets/Hennig.jpg" width="18%" class="fragment" style="background:none; border:none; box-shadow:none; float:right">

<div style="width: 60%;">

* &shy;<!-- .element: class="fragment" -->Willi Hennig
 * &shy;<!-- .element: class="fragment" -->*Phylogenetic Systematics*
 * &shy;<!-- .element: class="fragment" -->1955
* &shy;<!-- .element: class="fragment" -->Introduces the concept of "cladograms"
 * &shy;<!-- .element: class="fragment" -->Show relations between taxa
 * &shy;<!-- .element: class="fragment" -->Grouping is based on traits
 * &shy;<!-- .element: class="fragment" -->Similarities are assumed to be of common ancestry

</div>

<img src="pt1_assets/cladogram.png" width="19%" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: -250px;right: -10px;">

---

### Phylogenetics

<img src="pt1_assets/CS-E.jpg" width="20%" class="fragment" style="background:none; border:none; box-shadow:none; float:right">

<div style="width: 70%;">

* &shy;<!-- .element: class="fragment" -->Cavalli-Sforza & Edwards
 * &shy;<!-- .element: class="fragment" -->*Phylograms*
 * &shy;<!-- .element: class="fragment" -->1963

</div>

<img src="pt1_assets/CS_phylo.jpg" width="30%" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: -100px;right: 320px;">
<img src="pt1_assets/CS_migrations.jpg" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: -100px;left: -30px;">

---

### Phylogenetic concepts

<img class="fragment" src="pt1_assets/Phylo_01.png" style="background:none; border:none; box-shadow:none;">

|||

### Phylogenetic concepts

<img class="fragment" src="pt1_assets/Phylo_02.png" style="background:none; border:none; box-shadow:none;">

---

### Rooted Vs. Unrooted Trees

<img class="fragment" src="pt1_assets/tree_roots.png" style="background:none; border:none; box-shadow:none;">

---

### Endgame

[<img class="fragment" src="pt1_assets/pan-aves.jpg" style="background:none; border:none; box-shadow:none;">](pt1_assets/pan-aves_large.jpg)

<p class="fragment">(click for larger version)</p>

|||

### Endgame 2021

[Nextstrain](https://nextstrain.org/ncov/global)

---

### References

* [Mitochondrial DNA](https://ghr.nlm.nih.gov/mitochondrial-dna#)
* [Molecular phylogenetics for newcommers](https://link.springer.com/chapter/10.1007%2F10_2016_49) (Find the PDF on Moodle)
* [Short intro to phylogenetics on NCBI](https://www.ncbi.nlm.nih.gov/books/NBK21122/)
* [Theropoda evolution](https://doi.org/10.1016/j.cub.2014.08.034)

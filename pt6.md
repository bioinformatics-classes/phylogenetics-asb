### classes[10] = "Phylogenetics Pt. VI - Multi-gene analyses"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](logo-ESTB.png)

Francisco Pina Martins

---

### Summary

* The multi-gene "problem"
* Concatenation
* Coalescence
* Supertree

---

### The problem with multi-gene phylogenetic reconstruction

* &shy;<!-- .element: class="fragment" -->Low number of sequences result in few molecular characters
    * &shy;<!-- .element: class="fragment" -->Low resolution
    * &shy;<!-- .element: class="fragment" -->Poor support

&shy;<!-- .element: class="fragment" -->![Gene fragments](pt6_assets/fragments.png)

---

### The "supermatix" approach

* &shy;<!-- .element: class="fragment" -->Also known as "concatenation"
* &shy;<!-- .element: class="fragment" -->Combines sequences from multiple fragments into a single, large dataset
* &shy;<!-- .element: class="fragment" -->*Horizontal* concatenation

&shy;<!-- .element: class="fragment" -->![Gene fragments](pt6_assets/genefragments.png)

|||

### Alignment

* &shy;<!-- .element: class="fragment" -->Requires aligned gene fragments
* &shy;<!-- .element: class="fragment" -->Taxa names **must** be matched across fragments

&shy;<!-- .element: class="fragment" -->![Gene fragments](pt6_assets/concat_step1.png)

|||

### Mismatches

* &shy;<!-- .element: class="fragment" -->Reductionist (remove incomplete taxa)
&shy;<!-- .element: class="fragment" -->![Gene fragments](pt6_assets/reductionist.png)
* &shy;<!-- .element: class="fragment" -->Maximalist (add N's to missing fragments)
&shy;<!-- .element: class="fragment" -->![Gene fragments](pt6_assets/maximalist.png)

---

### Data partitioning

* &shy;<!-- .element: class="fragment" -->Each fragment is likely to follow a different evolutionary model
* &shy;<!-- .element: class="fragment" -->This is handled through *partitions*
    * &shy;<!-- .element: class="fragment" -->*MrBayes* uses a NEXUS propriety in the *mrbayes* block:
        * &shy;<!-- .element: class="fragment" -->`partition reptile = 4:
        1-722, 723-1508, 1509-2504, 2505-3383;`
    * &shy;<!-- .element: class="fragment" -->RAxML uses a `partitions` file - replace the model name with the filepath

<div class="fragment">

``` text
JC+G, p1 = 1-722, 2505-3383
HKY+F, p2 = 723-1508
GTR+I, p3 = 1509-2504
```

</div>

---

### Coalescence methods

* &shy;<!-- .element: class="fragment" -->Describe the **stochastic process** by which gene lineages merge (coalesce) backward in time to their most recent common ancestor
    * &shy;<!-- .element: class="fragment" -->Infer species trees from gene trees
    * &shy;<!-- .element: class="fragment" -->Works even in the presence of complex evolutionary processes

&shy;<!-- .element: class="fragment" -->[![Coalescent illustration](pt6_assets/coalescent.png)](http://dx.doi.org/10.13140/RG.2.2.12287.02725)

|||

### Coalescence software

* [fastsimcoal2](http://cmpg.unibe.ch/software/fastsimcoal2/)
* [BEAST2](https://www.beast2.org/)
* [Migraine](https://kimura.univ-montp2.fr/~rousset/Migraine.htm)

---

### The "supertree" approach

* &shy;<!-- .element: class="fragment" -->Used to infer *large-scale* evolutionary relationships among taxa
    * &shy;<!-- .element: class="fragment" -->Data too large to be concatenated
* &shy;<!-- .element: class="fragment" -->Merges **individual trees** in a single, composite tree
    * &shy;<!-- .element: class="fragment" -->Conflict resolution
        * &shy;<!-- .element: class="fragment" -->Consensus based approaches
        * &shy;<!-- .element: class="fragment" -->Weighting by support

&shy;<!-- .element: class="fragment" -->![Supertree](pt6_assets/trees.png)

|||

### "Supertree" software

* [Clann](http://chriscreevey.github.io/clann/)
* [ASTRAL](https://github.com/smirarab/ASTRAL/)
* [ASTER](https://github.com/chaoszhang/ASTER)
* [Dendropy](https://dendropy.readthedocs.io/en/main/)

---

### Final challenge

* Write a program that concatenates sequence data
    * Use any language you want
    * Input has to be provided in one (or more, optionally) of the following formats:
        * FASTA
        * Phylip
        * Nexus
* Document your program with a README.md file
* Your program needs to deal with missing sequences by either (optionally both):
    * Removing individuals not represented in all fragments
    * Adding 100% "N" sequences when an individual is not represented in a fragment
* All input must be passed through command line arguments

---

### References

* [SPLACE](https://doi.org/10.3389/fbinf.2022.1074802)
* [Apex](https://doi.org/10.1111%2F1755-0998.12567)
* [Concatenator](https://doi.org/10.1111/j.1755-0998.2008.02164.x)
* [Arenas & Posada, 2014](https://doi.org/10.1093/molbev/msu078)
* [Barriers to gene flow in marine coastal invertebrates: a multi-species and multi-scale approach](http://dx.doi.org/10.13140/RG.2.2.12287.02725)
* [Phylogenetic supertrees: Assembling the trees of life](https://doi.org/10.1016/S0169-5347(97)01242-1)

### classes[8] = "Phylogenetics Pt. IV - Evolutionary models + ML"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](logo-ESTB.png)

Francisco Pina Martins

---

### Summary

* &shy;<!-- .element: class="fragment" -->Evolutionary models
* &shy;<!-- .element: class="fragment" -->Maximum likelihood
* &shy;<!-- .element: class="fragment" -->Practical work

---

### Phylogenetic reconstruction methods

* &shy;<!-- .element: class="fragment" data-fragment-index="1"-->There are several methods for phylogenetic reconstruction
* &shy;<!-- .element: class="fragment" data-fragment-index="2"-->Those that use character states
    * &shy;<!-- .element: class="fragment" data-fragment-index="3"-->Maximum parsimony
    * &shy;<!-- .element: class="fragment" data-fragment-index="4"--><span class="fragment highlight-red" data-fragment-index="9">Maximum likelihood</span>
    * &shy;<!-- .element: class="fragment" data-fragment-index="5"-->Bayesian inference
* &shy;<!-- .element: class="fragment" data-fragment-index="6"-->Those that use distance measurements
    * &shy;<!-- .element: class="fragment" data-fragment-index="7"-->UPGMA
    * &shy;<!-- .element: class="fragment" data-fragment-index="8"-->Neighbour joining

---

### A problem with Parsimony

![Sequence saturation plot](pt4_assets/01saturation.png)

* &shy;<!-- .element: class="fragment" -->Sequence saturation

|||

### Sequence evolution and mutation accumulation

![Sequence evolution](pt4_assets/02differentiation.png)

|||

### Sequence evolution and mutation accumulation

![Mutation accumulation 01](pt4_assets/03differentiation_bases.png)

|||

### As time goes by...

![Mutation accumulation 02](pt4_assets/04time_goes_by.png)

|||

### However, thing are not always so simple

![Mutation accumulation 03](pt4_assets/05two_subs_one_diff.png)

* &shy;<!-- .element: class="fragment" -->2 substitutions, 1 difference

|||

### In many possible ways

![Mutation accumulation 04](pt4_assets/06two_subs_one_diff.png)

* &shy;<!-- .element: class="fragment" -->2 subs., 1 diff.

|||

### In many possible ways

![Mutation accumulation 05](pt4_assets/07two_subs_zero_diff.png)

* &shy;<!-- .element: class="fragment" -->2 subs., 0 diff.

|||

### In many possible ways

![Mutation accumulation 06](pt4_assets/08three_subs_zero_diff.png)

* &shy;<!-- .element: class="fragment" -->3 subs., 0 diff.

|||

### In many possible ways

![Mutation accumulation 07](pt4_assets/09two_subs_zero_diff.png)

* &shy;<!-- .element: class="fragment" -->2 subs., 0 diff.

|||

### Which takes us back to the start!

![Sequence saturation plot](pt4_assets/01saturation.png)

---

### Evolutionary models

* &shy;<!-- .element: class="fragment" -->Some models were thus devised to overcome the saturation issue
* &shy;<!-- .element: class="fragment" -->They are based on the *probability* of a determined change occurring over time
    * &shy;<!-- .element: class="fragment" -->*What is the probability of an A mutating into a T?*

&shy;<!-- .element: class="fragment" -->![Transitions vs. transversions](pt4_assets/Transitions_vs_transversions.png)

|||

### Substitution probability matrix

![Substitution probability matrix](pt4_assets/Sub_prob_matrix.png)

* &shy;<!-- .element: class="fragment" -->The probability of an *A* mutating into a *T* is given by <span style="color:#451bb6">*p*<sub>AT</sub></span>

|||

### And finally...

* &shy;<!-- .element: class="fragment" -->The probability matrix is then used to derive a *Maximum Likelihood* function that recalculates the difference between sequences
* &shy;<!-- .element: class="fragment" -->Mathematical demonstration in 17 simple steps:

&shy;<!-- .element: class="fragment" -->![Wreck it Ralph](pt4_assets/no_way.jpg)

---

### What about base frequencies?

* &shy;<!-- .element: class="fragment" -->Some evolutionary models consider all base frequencies to be the same:
    * &shy;<!-- .element: class="fragment" -->πA=0.25, πC=0.25, πG=0.25, πT=0.25
* &shy;<!-- .element: class="fragment" -->Others make this frequency vary according to the observed data

&shy;<!-- .element: class="fragment" -->![Different base frequencies barplot](pt4_assets/base_freqs.png)

---

### Model assumptions

![Warning, assumptions ahead](pt4_assets/Assumptions.jpg)

* &shy;<!-- .element: class="fragment" -->Constant substitution rate over time and across lineages
* &shy;<!-- .element: class="fragment" -->Sites mutate independently
    * &shy;<!-- .element: class="fragment" --><span class="fragment highlight-red">Probability of each position mutating is identical and constant across time</span>

|||

### But their mutation rate can be modeled...

![Gamma distribution in phylogenetics](pt4_assets/gamma.png)

---

### Choosing models

* &shy;<!-- .element: class="fragment" -->Many models were devised, throughout time, in increasing order of complexity
    * &shy;<!-- .element: class="fragment" -->[Jukes-Cantor](http://treethinkers.org/jukes-cantor-model-of-dna-substitution/)
    * &shy;<!-- .element: class="fragment" -->[Kimura-2-Parameter](https://www.megasoftware.net/mega4/WebHelp/part_iv___evolutionary_analysis/computing_evolutionary_distances/distance_models/nucleotide_substitution_models/hc_kimura_2_parameter_distance.htm)
    * &shy;<!-- .element: class="fragment" -->[Hasegawa, Kishino & Yano](https://www.ncbi.nlm.nih.gov/pubmed/3934395)
    * &shy;<!-- .element: class="fragment" -->[GTR](https://doi.org/10.1016/S0022-5193(05)80104-3)
* &shy;<!-- .element: class="fragment" -->There is software to test and choose the best model to fit the data

&shy;<!-- .element: class="fragment" -->![GTR Maximum likelihood function formula](pt4_assets/GTR.png)

|||

### Choosing models

* &shy;<!-- .element: class="fragment" -->Model choice has many nuances
    * &shy;<!-- .element: class="fragment" -->All models must be tested
    * &shy;<!-- .element: class="fragment" -->All models must be compared
* &shy;<!-- .element: class="fragment" -->[Modeltest NG](https://github.com/ddarriba/modeltest)
* &shy;<!-- .element: class="fragment" -->[IQTree](http://www.iqtree.org/)

&shy;<!-- .element: class="fragment" -->![Modeltest-ng screenshot](pt4_assets/modeltest-ng.png)

---

### Maximum Likelihood

* &shy;<!-- .element: class="fragment" -->Best explanation for observed outcome
* &shy;<!-- .element: class="fragment" -->Requires:  
    * &shy;<!-- .element: class="fragment" -->Sequence data
    * &shy;<!-- .element: class="fragment" -->Evolutionary model
    * &shy;<!-- .element: class="fragment" -->Tree (topology and branch lengths) 
* &shy;<!-- .element: class="fragment" -->For any given topology, what set of branch lengths makes the observed data most likely?
* &shy;<!-- .element: class="fragment" -->Which of all possible trees has the greatest likelihood?

&shy;<!-- .element: class="fragment" -->[![XKCD comic #1831](pt4_assets/here_to_help.png)](https://xkcd.com/1831/)

|||

### Maximum Likelihood

![Maximum likelihood scheme](pt4_assets/ML_trees.png)

|||

### Maximum Likelihood

* &shy;<!-- .element: class="fragment" -->Assert the probability of each possible combination on each site
* &shy;<!-- .element: class="fragment" -->...for every site
    * &shy;<!-- .element: class="fragment" -->Then sum the partial likelihoods to get a total likelihood
* &shy;<!-- .element: class="fragment" -->...for every tree
    * &shy;<!-- .element: class="fragment" -->And the best likelihood is chosen as the best tree

---

### Tree branch support

* &shy;<!-- .element: class="fragment" -->Have you noticed a number above each branch on trees?
* &shy;<!-- .element: class="fragment" -->This number indicates how well supported each branch is
    * &shy;<!-- .element: class="fragment" -->Arbitrary value
    * &shy;<!-- .element: class="fragment" -->Represents *precision*, not accuracy  
* &shy;<!-- .element: class="fragment" -->How are these obtained?

|||

### Tree branch support

![Bootstrapping scheme](pt4_assets/bootstrap.png)

---

### Practical work

* Install the software [modeltest-ng](https://github.com/ddarriba/modeltest)
* Find out the most appropriate model for each of the 3 gene fragments used in Akihito *et al.* 2016 ([Modeltest-ng Wiki should have all you need](https://github.com/ddarriba/modeltest/wiki/))
* Install the software [raxml-ng](https://github.com/amkozlov/raxml-ng)
    * [Here](https://github.com/amkozlov/raxml-ng/wiki/Tutorial) is the respective tutorial
* Build a Maximum Likelihood tree for each of the 3 gene fragments used in Akihito *et al.* 2016
* Interpret the trees (consult the original paper if you have to)
* Don't forget to obtain bootstrap values!
* You can find a copy of the data [here](pt3_assets/Akihito_data.tar.xz) in case you have lost yours for some reason

---

### References

* [Sequence saturation](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3057953/)
* [Evolutionary models](https://evomics.org/resources/substitution-models/nucleotide-substitution-models/)
* [Maximum Likelihood inference](https://scholarship.claremont.edu/cgi/viewcontent.cgi?article=1047&context=scripps_theses) (especially the introduction)
* [Darriba *et al.* 2020](doi.org/10.1093/molbev/msz189)
* [Kozlov *et al.* 2019](https://doi.org/10.1093/bioinformatics/btz305)

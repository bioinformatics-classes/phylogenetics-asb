### classes[6] = "Phylogenetics Pt. II - Alignments"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](logo-ESTB.png)

Francisco Pina Martins

---

### Summary


* &shy;<!-- .element: class="fragment" -->Why alignments
* &shy;<!-- .element: class="fragment" -->Alignments algorithms
* &shy;<!-- .element: class="fragment" -->Which to use and when (blast, mafft & clustal, vsearch)
* &shy;<!-- .element: class="fragment" -->Practical work


---

### Alignment concept

* &shy;<!-- .element: class="fragment" -->Phylogenetic analyses are about performing comparisons
* &shy;<!-- .element: class="fragment" -->We have to be sure what we have is comparable
    * &shy;<!-- .element: class="fragment" -->[*Homology*](https://bio.libretexts.org/Bookshelves/Microbiology/Microbiology_(Boundless)/07%3A_Microbial_Genetics/7.13%3A_Bioinformatics/7.13C%3A_Homologs_Orthologs_and_Paralogs) is required
    * &shy;<!-- .element: class="fragment" -->Each of our *characters* has to be homologous

&shy;<!-- .element: class="fragment" -->![Aligned and unaligned sequences](pt2_assets/unalign_vs_align.png)

---

### Sequence variation

* &shy;<!-- .element: class="fragment" -->Sequences may diverge from a **common ancestor** in 3 different ways
    * &shy;<!-- .element: class="fragment" -->Substitutions: (<font color="green">ACT<font color="red">G</font> <font color="navy">-></font> ACT<font color=red>C</font></font>)
    * &shy;<!-- .element: class="fragment" -->Insertions: (<font color="green">ACTG <font color="navy">-></font> ACTG<font color=red>TTGG</font></font>)
    * &shy;<!-- .element: class="fragment" -->Deletion: (<font color="green">ACTG <font color="navy">-></font> A</font>)
* &shy;<!-- .element: class="fragment" -->What happens when we compare sequences of different length?

---

### Scoring alignments

* &shy;<!-- .element: class="fragment" -->Let's try to align "ALI" and "IGN"


<div class="fragment" style="witdth=60%">

```text
ALI | -ALI | --ALI | ALI- | ALI-- | A-LI | AL-I | ALI- | ALI- 
IGN | IGN- | IGN-- | -IGN | --IGN | IGN- | IGN- | I-GN | IG-N ...
```

</div>

* &shy;<!-- .element: class="fragment" -->Each alignment has to be scored

&shy;<!-- .element: class="fragment" -->![High score](pt2_assets/newhighscore.jpg)

|||

### Scoring alignments

* &shy;<!-- .element: class="fragment" -->Gap penalty function
    * &shy;<!-- .element: class="fragment" -->*w*(*k*) indicates the cost of opening a gap of length *k*
* &shy;<!-- .element: class="fragment" -->Substitution matrix
    * &shy;<!-- .element: class="fragment" -->*s*(*a*,*b*) indicates the cost of aligning *a* with *b*
* &shy;<!-- .element: class="fragment" -->Different algorithms will score these in different ways

---

### Alignment algorithms

* &shy;<!-- .element: class="fragment" -->There is a plethora of available alignment algorithms
    * &shy;<!-- .element: class="fragment" -->Smith-Waterman
    * &shy;<!-- .element: class="fragment" -->BLAST
    * &shy;<!-- .element: class="fragment" -->Clustal
    * &shy;<!-- .element: class="fragment" -->vsearch
    * &shy;<!-- .element: class="fragment" -->mafft
* &shy;<!-- .element: class="fragment" -->There are many, many other
* &shy;<!-- .element: class="fragment" -->All have a place (some in history)

&shy;<!-- .element: class="fragment" -->![Trophy](pt2_assets/trophy.jpg)

|||

### Alignment algorithms

* &shy;<!-- .element: class="fragment" -->Smith-Waterman
    * &shy;<!-- .element: class="fragment" -->Find optimum local alignments (slow & accurate)
* &shy;<!-- .element: class="fragment" -->BLAST
    * &shy;<!-- .element: class="fragment" -->Heuristic approach to S-W (faster, at cost of accuracy)
* &shy;<!-- .element: class="fragment" -->Clustal
    * &shy;<!-- .element: class="fragment" -->Progressive alignment - slow, inaccurate when dealing with multiple gaps
* &shy;<!-- .element: class="fragment" -->vsearch
    * &shy;<!-- .element: class="fragment" -->Fast & accurate, ideal for finding optimum local alignments
* &shy;<!-- .element: class="fragment" -->mafft
    * &shy;<!-- .element: class="fragment" -->Iterative method with consistency based scoring approach - fast & accurate

---

### Who and when?

* &shy;<!-- .element: class="fragment" -->Find a sequence's best match to a large pool of candidates
    * &shy;<!-- .element: class="fragment" -->Smith-Waterman
    * &shy;<!-- .element: class="fragment" -->BLAST
    * &shy;<!-- .element: class="fragment" -->vsearch
* &shy;<!-- .element: class="fragment" -->Align multiple sequences
    * &shy;<!-- .element: class="fragment" -->Clustal
    * &shy;<!-- .element: class="fragment" -->mafft
    * &shy;<!-- .element: class="fragment" -->muscle

---

### Your task for today

* &shy;<!-- .element: class="fragment" -->Obtain all sequences used in [*Akihito et al. 2016*](https://www.sciencedirect.com/science/article/pii/S0378111915012226)
    * &shy;<!-- .element: class="fragment" -->You will find this paper in Moodle
* &shy;<!-- .element: class="fragment" -->Align these sequences in 3 separate FASTA files - one per gene fragment
* &shy;<!-- .element: class="fragment" -->*Hint #1:* You can use the fields [author] and [year] in the *Entrez* search string
* &shy;<!-- .element: class="fragment" -->*Hint #2:* You can search *ranges* like this: `AY123:AY130[accn]`
* &shy;<!-- .element: class="fragment" -->*Hint #3:* Use *mafft* to align your sequences

&shy;<!-- .element: class="fragment" -->![Japan flag](pt2_assets/japan.jpg)

---

### References

* <a href="https://bio.libretexts.org/Bookshelves/Microbiology/Microbiology_(Boundless)/07%3A_Microbial_Genetics/7.13%3A_Bioinformatics/7.13C%3A_Homologs_Orthologs_and_Paralogs">Homology concept</a>
* [Mutation types](https://evolution.berkeley.edu/dna-and-mutations/types-of-mutations/)
* [Sequence alignment "behind the scenes"](https://www.bioinformaticshome.com/bioinformatics_tutorials/sequence_alignment/DNA_scoring_matrices.html)
* [*Akihito et al. 2016*](https://www.sciencedirect.com/science/article/pii/S0378111915012226)
* [BLAST algorithm](https://blastalgorithm.com/)
* [mafft (and references therein)](https://mafft.cbrc.jp/alignment/software/)


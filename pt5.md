### classes[9] = "Phylogenetics Pt. V - Bayesian Inference"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](logo-ESTB.png)

Francisco Pina Martins

---

### Summary

* &shy;<!-- .element: class="fragment" -->Bayesian Inference
* &shy;<!-- .element: class="fragment" -->MCMCMC based tree construction
* &shy;<!-- .element: class="fragment" -->Practical work

---

### Phylogenetic reconstruction methods

* &shy;<!-- .element: class="fragment" data-fragment-index="1"-->There are several methods for phylogenetic reconstruction
* &shy;<!-- .element: class="fragment" data-fragment-index="2"-->Those that use character states
    * &shy;<!-- .element: class="fragment" data-fragment-index="3"-->Maximum parsimony
    * &shy;<!-- .element: class="fragment" data-fragment-index="4"-->Maximum likelihood
    * &shy;<!-- .element: class="fragment" data-fragment-index="5"--><span class="fragment highlight-red" data-fragment-index="9">Bayesian inference</span>
* &shy;<!-- .element: class="fragment" data-fragment-index="6"-->Those that use distance measurements
    * &shy;<!-- .element: class="fragment" data-fragment-index="7"-->UPGMA
    * &shy;<!-- .element: class="fragment" data-fragment-index="8"-->Neighbour joining

---

### How does Bayesian inference work?

<img src="pt5_assets/bayes.gif" style="background:none; border:none; box-shadow:none; float:right">

* &shy;<!-- .element: class="fragment" -->Consider H<sub>1</sub> and H<sub>2</sub>
* &shy;<!-- .element: class="fragment" -->It has the following Likelihood ratio:
&shy;<!-- .element: class="fragment" -->`$$ \frac{Prob(D|H_1)}{Prob(D|H_2)} = 1/2 $$`
    * &shy;<!-- .element: class="fragment" -->These results favour H<sub>2</sub> at a 1:2 ratio
* &shy;<!-- .element: class="fragment" -->Let's assume a *Prior* of 3/2:
    * &shy;<!-- .element: class="fragment" -->Reclaculate the ratio:
&shy;<!-- .element: class="fragment" -->`$$ (1/2) \times (3/2) = 3/4 $$`
    * &shy;<!-- .element: class="fragment" -->Results still favour H<sub>2</sub>, but at a 3:4 ratio
    * &shy;<!-- .element: class="fragment" -->3/4 is the *Posterior probability*

|||

### The Bayes Theorem

`$$ Prob(H|D) = \frac{Prob(H) \times Prob(D|H)}{\sum_H Prob(H) \times Prob(D|H)} $$`

---

### The effect of Priors

<img src="pt5_assets/priors.png" style="background:none; border:none; box-shadow:none;">

|||

### Intromission

Let's do this the hard way

([Here](pt5_assets/Prior_effects.R) is the code for the plots in the presentation)

---

### Tree building

* &shy;<!-- .element: class="fragment" -->BI finds the best tree using an MCMCMC approach
    * &shy;<!-- .element: class="fragment" -->**M**etropolis **C**oupled **M**arkov **C**hain **M**onte **C**arlo
    * &shy;<!-- .element: class="fragment" -->Iterative approach

<img src="pt5_assets/monte_carlo.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Markov Chain Monte Carlo

1. &shy;<!-- .element: class="fragment" -->Start with a random tree *T<sub>i</sub>*
2. &shy;<!-- .element: class="fragment" -->Pick and propose a neighbour tree *T<sub>j</sub>*
3. &shy;<!-- .element: class="fragment" -->Compute the trees' probability density functions:
&shy;<!-- .element: class="fragment" -->`$$ R = \frac{f(T_j)}{f(T_i)} $$`
4. &shy;<!-- .element: class="fragment" -->If *R >= 1*, accept the new tree
5. &shy;<!-- .element: class="fragment" -->Else, draw a random number "*W*" from a uniform distribution
 1. &shy;<!-- .element: class="fragment" -->If *W < R*, accept the new tree
 2. &shy;<!-- .element: class="fragment" -->Else, keep the old tree
6. &shy;<!-- .element: class="fragment" -->Return to step 2

|||

### Markov Chain Monte Carlo

<img src="pt5_assets/MCMCMC.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Markov Chain Monte Carlo

<div style="position:relative; width:800px; height:600px; margin:0 auto;">
  <img class="fragment fade-out" data-fragment-index="0" width="800" height="600" src="pt5_assets/likelihoods.png" style="position:absolute;top:0;left:0;background:none;border:none;box-shadow:none;" />
    <img class="fragment fade-in" data-fragment-index="0" width="800" height="600" src="pt5_assets/likelihoods_2.png" style="position:absolute;top:0;left:0;background:none;border:none;box-shadow:none;" />
    </div>

---

### Evolutionary models in Bayesian inference

* &shy;<!-- .element: class="fragment" -->The most used implementation of this method is `MrBayes`
* &shy;<!-- .element: class="fragment" -->`MrBayes` can use evolutionary models by setting 2 parameters:
    * &shy;<!-- .element: class="fragment" -->`nst=X` (sets the number of possible substitutions)
    * &shy;<!-- .element: class="fragment" -->`prset=XXX` (sets the prior distribution and nucleotide rates)
* &shy;<!-- .element: class="fragment" -->Understanding and modifying these is beyond the scope of this course

---

### Support values

* &shy;<!-- .element: class="fragment" -->In Bayesian inference they are provided by *posterior probability* values
    * &shy;<!-- .element: class="fragment" -->Range from 0 to 1
    * &shy;<!-- .element: class="fragment" -->Higher values mean higher confidence
* &shy;<!-- .element: class="fragment" -->Are obtained from the consensus of the best trees

&shy;<!-- .element: class="fragment" -->[![Bayesian Tree](pt5_assets/Bayesian_tree.png)](https://doi.org/10.1186/s12862-022-02037-2)

---

### Practical work

* Install the software [mrbayes](http://nbisweden.github.io/MrBayes/)
* Build a Bayesian Inference tree on each of the 3 gene fragments used in Akihito *et al.* 2016
* Interpret the trees (consult the original paper if you have to)
* Do not forget to look at the Posterior probability values!
* You can find a copy of the data [here](pt3_assets/Akihito_data.tar.xz) in case you have lost yours for some reason
* Use any program of your choice to view and manipulate the resulting trees
* *Hint*: You can include MrBayes commands in the NEXUS file. [Here](pt5_assets/12sV2.nex) is an example for you to adapt
 * This will *automate* your analysis

---

### References

* [Count Bayesie](https://www.countbayesie.com/blog/2015/2/18/hans-solo-and-bayesian-priors)
* [MrBayes manual](https://github.com/NBISweden/MrBayes/blob/develop/doc/manual/Manual_MrBayes_v3.2.pdf)
* [Evolutionary models in MrBayes](http://mrbayes.sourceforge.net/wiki/index.php/Tutorial_3.2#Specifying_a_Model)
